<?php

namespace app\controllers;

use \Yii;
use yii\helpers\Url;
use yii\web\Controller;

class CController extends Controller
{

    public function beforeAction($event)
    {
        if ((Yii::$app->controller->action->id == 'edit' || Yii::$app->controller->action->id == 'create') &&
            (Yii::$app->request->get('user_id') && Yii::$app->request->get('user_id') != Yii::$app->user->id)) {
           $this->redirect(Url::previous());
        } else {
            Url::remember();
        }

        if (!isset($_GET['welcome'])) {
            Yii::$app->response->cookies->remove('welcome');
        }

        return parent::beforeAction($event);
    }

}