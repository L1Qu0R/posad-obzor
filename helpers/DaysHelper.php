<?php

namespace app\helpers;

use \Yii;

class DaysHelper
{

    public static function replaceDays($days_array)
    {
        $days = [];

        foreach ($days_array as $day_num) {
            if ($day_num == 1) $days[] = 'понедельник';
            if ($day_num == 2) $days[] = 'вторник';
            if ($day_num == 3) $days[] = 'среда';
            if ($day_num == 4) $days[] = 'четверг';
            if ($day_num == 5) $days[] = 'пятница';
            if ($day_num == 6) $days[] = 'суббота';
            if ($day_num == 7) $days[] = 'воскресенье';
        }

        return $days;
    }

}