<?php

use yii\db\Migration;

class m150224_082906_create_company_company_category_table extends Migration
{
    public function up()
    {
        $this->createTable('company_company_category', [
            'id' => 'int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'company_id' => 'int(11) UNSIGNED NOT NULL',
            'category_id' => 'int(11) UNSIGNED NOT NULL',
            'subcategory_id' => 'int(11) UNSIGNED NOT NULL',
        ]);

        $this->addForeignKey('company_id', 'company_company_category', 'company_id', 'company', 'id');
        $this->addForeignKey('category_id', 'company_company_category', 'category_id', 'company_category', 'id');
        $this->addForeignKey('subcategory_id', 'company_company_category', 'subcategory_id', 'company_category', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('company_id', 'company_company_category');
        $this->dropForeignKey('category_id', 'company_company_category');
        $this->dropForeignKey('subcategory_id', 'company_company_category');

        $this->dropTable('company_company_category');

        return false;
    }
}
