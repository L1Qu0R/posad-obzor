<?php

use yii\db\Schema;
use yii\db\Migration;

class m150219_125203_create_company_category_table extends Migration
{
    public function up()
    {
        $this->createTable('company_category', [
            'id' => 'int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'name' => 'varchar(255) NOT NULL DEFAULT ""',
            'parent_id' => 'int(11) UNSIGNED NOT NULL',
        ]);
    }

    public function down()
    {
        $this->dropTable('company_category');

        return false;
    }
}
