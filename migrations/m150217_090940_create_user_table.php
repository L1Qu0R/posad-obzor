<?php

use yii\db\Migration;

class m150217_090940_create_user_table extends Migration
{
    public function up()
    {
        $this->createTable('user', [
            'id' => 'int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'name' => 'varchar(255) NOT NULL DEFAULT ""',
            'email' => 'varchar(255) NOT NULL DEFAULT ""',
            'salt' => 'varchar(255) NOT NULL DEFAULT ""',
            'password' => 'varchar(255) NOT NULL DEFAULT ""',
            'main_number' => 'varchar(255) NOT NULL DEFAULT ""',
            'extra_number' => 'varchar(255) NOT NULL DEFAULT ""',
            'vk' => 'varchar(255) NOT NULL DEFAULT ""',
            'fb' => 'varchar(255) NOT NULL DEFAULT ""',
            'tw' => 'varchar(255) NOT NULL DEFAULT ""',
            'mail' => 'varchar(255) NOT NULL DEFAULT ""',
            'c_date' => 'DATETIME NOT NULL DEFAULT "0000-00-00 00:00:00"',
        ]);
    }

    public function down()
    {
        $this->dropTable('user');

        return false;
    }
}
