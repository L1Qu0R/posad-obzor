<?php

use yii\db\Schema;
use yii\db\Migration;

class m150219_125546_create_company_table extends Migration
{
    public function up()
    {
        $this->createTable('company', [
            'id' => 'int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'name' => 'varchar(255) NOT NULL DEFAULT ""',
            'type_id' => 'int(11) UNSIGNED NOT NULL',
            'short_description' => 'varchar(255) NOT NULL DEFAULT ""',
            'description' => 'text',
            'category_id' => 'int(11) UNSIGNED NOT NULL',
            'subcategory_id' => 'int(11) UNSIGNED NOT NULL',
            'working_hours' => 'varchar(255) NOT NULL DEFAULT ""',
            'working_days' => 'varchar(255) NOT NULL DEFAULT ""',
            'city_id' => 'int(11) UNSIGNED NOT NULL',
            'street' => 'varchar(255) NOT NULL DEFAULT ""',
            'house' => 'varchar(255) NOT NULL DEFAULT ""',
            'office' => 'varchar(255) NOT NULL DEFAULT ""',
            'main_number' => 'varchar(255) NOT NULL DEFAULT ""',
            'extra_number' => 'varchar(255) NOT NULL DEFAULT ""',
            'site_url' => 'varchar(255) NOT NULL DEFAULT ""',
            'email' => 'varchar(255) NOT NULL DEFAULT ""',
            'vk' => 'varchar(255) NOT NULL DEFAULT ""',
            'fb' => 'varchar(255) NOT NULL DEFAULT ""',
            'tw' => 'varchar(255) NOT NULL DEFAULT ""',
            'inst' => 'varchar(255) NOT NULL DEFAULT ""',
            'ok' => 'varchar(255) NOT NULL DEFAULT ""',
            'mail' => 'varchar(255) NOT NULL DEFAULT ""',
            'c_date' => 'DATETIME NOT NULL DEFAULT "0000-00-00 00:00:00"',
            'e_date' => 'DATETIME NOT NULL DEFAULT "0000-00-00 00:00:00"',
        ]);

        $this->addForeignKey('type_id', 'company', 'type_id', 'company_type', 'id');
        $this->addForeignKey('category_id', 'company', 'category_id', 'company_category', 'id');
        $this->addForeignKey('subcategory_id', 'company', 'subcategory_id', 'company_category', 'id');
        $this->addForeignKey('city_id', 'company', 'city_id', 'city', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('type_id', 'company');
        $this->dropForeignKey('category_id', 'company');
        $this->dropForeignKey('subcategory_id', 'company');
        $this->dropForeignKey('city_id', 'company');

        $this->dropTable('company');

        return false;
    }
}
