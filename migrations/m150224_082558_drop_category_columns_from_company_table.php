<?php

use yii\db\Schema;
use yii\db\Migration;

class m150224_082558_drop_category_columns_from_company_table extends Migration
{
    public function up()
    {
        $this->dropForeignKey('category_id', 'company');
        $this->dropForeignKey('subcategory_id', 'company');

        $this->dropColumn('company', 'category_id');
        $this->dropColumn('company', 'subcategory_id');
    }

    public function down()
    {
        $this->addColumn('company', 'category_id', 'int(11) UNSIGNED NOT NULL');
        $this->addColumn('company', 'subcategory_id', 'int(11) UNSIGNED NOT NULL');

        $this->addForeignKey('category_id', 'company', 'category_id', 'company_category', 'id');
        $this->addForeignKey('subcategory_id', 'company', 'subcategory_id', 'company_category', 'id');

        return false;
    }
}
