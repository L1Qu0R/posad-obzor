<?php

namespace app\modules\admin\widgets;

use yii\base\Widget;

class MenuWidget extends Widget
{
    public function run()
    {
        return $this->render('menu_widget');
    }
}