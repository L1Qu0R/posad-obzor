<?php

use yii\helpers\Html;
use app\modules\admin\AppAsset;

/* @var $content string */

AppAsset::register($this);
$model = new \app\models\User();
$login_form = new \app\models\LoginForm();

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>

    <?= Html::csrfMetaTags() ?>
    <title>Главная / <?= Html::encode(Yii::$app->name) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="container">
    <?= $content; ?>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
