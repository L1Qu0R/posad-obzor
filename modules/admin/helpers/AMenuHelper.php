<?php

namespace app\modules\admin\helpers;

use Yii;
use yii\helpers\FileHelper;

class AMenuHelper
{
    public static function getMenu()
    {
        $models = $models = FileHelper::findFiles(Yii::getAlias('@app/models'));
        $models_array = [];

        foreach ($models as $model) {
            $model = explode(DIRECTORY_SEPARATOR, $model);
            $model = end($model);
            $model = explode('.', $model)[0];
            $model_namespaced = "\\app\\models\\" . $model;

            if ($model_namespaced::RUS_NAME && $model_namespaced::MENU_SORT) {
                $models_array[$model_namespaced::MENU_SORT] = ['en' => strtolower($model), 'ru' => $model_namespaced::RUS_NAME];
            }

            ksort($models_array);
        }

        return $models_array;
    }

    public static function isActiveModel($model, $param)
    {
        if (isset($_GET[$param])) {
            $active = ($model == $_GET[$param]) ? 'active' : '';

            return $active;
        } else if ($model === false && $param === false) {
            return 'active';
        } else {
            return '';
        }
    }

    public static function getUrlElement($number)
    {
        $url = Yii::$app->request->url;

        if (isset(explode('/', $url)[$number]))  {
            return explode('/', $url)[$number];
        } else {
            return false;
        }
    }
}