<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\CompanyCategory;

/* @var $this yii\web\View */
/* @var $model app\models\CompanyCategory */
/* @var $form yii\widgets\ActiveForm */

$empty_value = [0 => '-'];
$parent_category = ArrayHelper::map(CompanyCategory::find($model->parent_id)->where(['parent_id' => '0'])->all(), 'id', 'name');
$parent_category = $empty_value + $parent_category;
?>

<div class="company-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parent_id', ['template' => '{label}{input}'])->dropDownList($parent_category) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'alias')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
