<?php

use yii\helpers\Html;
use app\models\CompanyCategory;

?>
<tr>
    <td><?= $model->id ?></td>
    <td><?= ($model->parent_id != 0) ? CompanyCategory::find()->where(['id' => $model->parent_id])->one()->name : '-' ?></td>
    <td><?= $model->name ?></td>
    <td><?= $model->alias ?></td>
    <td>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', array('/admin/default/update',
            'module' => 'companycategory',
            'id' => $model->id)); ?>
    </td>
    <td>
        <?= Html::a('<span class="glyphicon glyphicon-remove"></span>', array('/admin/default/delete',
            'module' => 'companycategory',
            'id' => $model->id),array(
            'class' => 'delete',
        )); ?>
    </td>
</tr>