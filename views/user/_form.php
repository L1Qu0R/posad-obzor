<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'salt')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'main_number')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'extra_number')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'vk')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'fb')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'tw')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'mail')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'c_date')->textInput() ?>

    <?= $form->field($model, 'auth_key')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
