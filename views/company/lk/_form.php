<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;
use app\models\CompanyType;
use app\models\City;
use app\models\CompanyCompanyCategory;
use app\models\Company;
use app\helpers\DaysHelper;
use dosamigos\fileupload\FileUpload;

/* @var $this yii\web\View */
/* @var $model app\models\Company */
/* @var $form yii\widgets\ActiveForm */

$company_company_category = new CompanyCompanyCategory();
$last_company = Company::find()->orderBy('id DESC')->one();
$company_id = ($last_company) ? $last_company->id : 1;
$working_hours_from = ($model->working_hours != '') ? json_decode($model->working_hours)->from : '';
$working_hours_to = ($model->working_hours != '') ? json_decode($model->working_hours)->to : '';
$working_days = ($model->working_days != '') ? join(',', DaysHelper::replaceDays(json_decode($model->working_days))) : '';
$social = ($model->id != '') ?
    array_filter([
        'vk' => $model->vk,
        'fb' => $model->fb,
        'tw' => $model->tw,
        'inst' => $model->inst,
        'ok' => $model->ok,
        'mail' => $model->mail
    ]) : '';
$rubric = ($model->id != '') ? CompanyCompanyCategory::find()->where(['company_id' => $model->id])->all() : '';
$company_gallery_id = ($model->id != '') ? $model->id : $last_company->id + 1;

?>

<div class="content-right-block">
    <div class="content-right-block-company">
        <?php $form = ActiveForm::begin([
            'options' => ['enctype'=>'multipart/form-data'],
            'enableAjaxValidation' => false,
            'enableClientValidation' => false,
            'fieldConfig' => [
                'template' => "{input}\n{error}",
            ],
        ]); ?>

        <div class="content-right-block-company-logo">
            <input type="hidden" name="company_id" />
            <?php if (file_exists(Yii::getAlias('@webroot') . '/upload/company/'.$model->id.'/logo.png')): ?>
                <img id="logo-img" src="/upload/company/<?=$model->id?>/logo.png" alt="" />
            <?php else: ?>
                <img id="logo-img" src="/images/company_blank.png" alt="" />
            <?php endif; ?>
            <?= $form->field($model, 'logo')->fileInput(['id' => 'company-logo', 'class' => 'image-upload']) ?>
            <label for="company-logo" class="content-right-block-company-logo__upload">загрузить логотип</label>
        </div>

        <div class="content-right-block-company-right">
            <div class="content-right-block-company-label">Название компании <span class="star">*</span></div>
            <?= $form->field($model, 'type_id', ['template' => '<div class="select small">{input}</div>'])->dropDownList(
                ArrayHelper::map(CompanyType::find()->all(), 'id', 'name')
            )?>
            <?= $form->field($model, 'name')->textInput(['maxlength' => 255, 'placeholder' => 'InQuality']) ?>

            <div class="content-right-block-company-label">Краткое описание <span class="star">*</span></div>
            <?= $form->field($model, 'short_description')->textInput(['maxlength' => 255, 'placeholder' => 'Веб разработка, дизайн']) ?>
        </div>

        <div class="content-right-block-company-label">Полное описание <span class="star">*</span></div>
        <?= $form->field($model, 'description')->textarea(['rows' => 6, 'placeholder' => 'Написать подробное описание компании']) ?>

        <div class="content-right-block-company-category">
            <div class="content-right-block-company-label">Сферы деятельности <span class="star">*</span></div>
            <div class="form-group">
                <?php if (!empty($rubric)): ?>
                    <?php foreach ($rubric as $rubric_key => $rubric_item): ?>
                        <div class="rubric" id="<?=$rubric_key?>">
                            <?= Html::textInput('',
                                $rubric_item->getCategory(['id' => $rubric_item->category_id])->one()->name.' -> '.
                                $rubric_item->getSubCategory(['id' => $rubric_item->subcategory_id])->one()->name,
                                ['placeholder' => 'Выбрать из списка', 'readonly' => 'true']) ?>
                            <div data-modal-id="category" class="button-ref open-lk-modal">Выбрать</div>
                            <?php if (count($rubric) > 1): ?>
                                <div class="rubric-remove" data-rubric-id="<?=$rubric_item->id?>"></div>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <div class="rubric" id="0">
                        <?= Html::textInput('', '', ['placeholder' => 'Выбрать из списка', 'readonly' => 'true']) ?>
                        <div data-modal-id="category" class="button-ref open-lk-modal">Выбрать</div>
                    </div>
                <?php endif; ?>
                <div class="content-right-block-company-category__append">Добавить рубрику</div>
            </div>
        </div>

        <div class="content-right-block-company-mode">
            <div class="content-right-block-company-label">Режим работы <span class="star">*</span></div>
            <div class="content-right-block-company-mode__item">
                <span>С</span><?= Html::textInput('', $working_hours_from, ['class' => 'from lcalendar_time', 'placeholder' => '10:00', 'readonly' => 'true']) ?>
            </div>
            <div class="content-right-block-company-mode__item">
                <span>До</span><?= Html::textInput('', $working_hours_to, ['class' => 'to lcalendar_time', 'placeholder' => '20:00', 'readonly' => 'true']) ?>
            </div>
            <div class="content-right-block-company-mode__item">
                <span>Выходной</span><?= Html::textInput('', $working_days, ['id' => 'weekend', 'placeholder' => 'суббота, воскресенье', 'readonly' => 'true']) ?><div data-modal-id="working-days" class="button-ref open-lk-modal">Выбрать</div>
            </div>
        </div>

        <div class="content-right-block-company-gallery">
            <div class="content-right-block-company-label">Фото галерея</div>
            <div class="content-right-block-company-gallery__item">
                <div class="content-right-block-company-gallery__item-controls">
                    <?= Html::textInput('', '', ['placeholder' => 'Укажите путь', 'readonly' => 'true']) ?>
                    <label class="button-ref" for="company-gallery_image">Выбрать</label>
                    <div class="content-right-block-company-gallery__item-controls-error">Максимальное количество изображений 12</div>
                    <?= FileUpload::widget([
                        'model' => $model,
                        'attribute' => 'gallery_image',
                        'url' => ['company/upload', 'id' => $company_gallery_id, 'type' => 'g'],
                        'options' => ['accept' => 'image/*', 'multiple' => true],
                        'clientOptions' => [
                            'maxFileSize' => 2000000
                        ],
                        'clientEvents' => [
                            'fileuploaddone' => 'function(e, data) {
                                console.log(e);
                                console.log(data);

                                var images_count = $(".content-right-block-company-gallery__item-previews").attr("data-images-count");
                                var upload_count = $(".content-right-block-company-gallery__item-previews").attr("data-upload-count");
                                var total_count = $(".content-right-block-company-gallery__item-previews").attr("data-total-count");

                                if ($(".content-right-block-company-gallery__item-previews article").length == 0) {
                                    var image_id = 1;
                                } else {
                                    var image_id = parseInt($(".content-right-block-company-gallery__item-previews article")
                                        .last().find("img").attr("id")) + 1;
                                }

                                if (images_count == "" || images_count == undefined || images_count <= 11) {
                                    $(".content-right-block-company-gallery__item-previews").append(
                                        "<article data-company-id='.$company_gallery_id.'>" +
                                            "<span></span>" +
                                            "<img id="+image_id+" src=\"/upload/company/'.$company_gallery_id.'/gallery/66/"+image_id+".png?lastmod='.time().'\" alt=\"\"/>" +
                                        "</article>");
                                    $(".content-right-block-company-gallery__item-previews").attr("data-images-count", parseInt(images_count) + 1);
                                } else {
                                    $(".content-right-block-company-gallery__item-controls-error").addClass("active");
                                }

                                if (total_count == (parseInt(images_count) + 1)) {
                                    $(".content-right-block-company-gallery__item-load").hide();
                                    $(".content-right-block-company-gallery__item-previews").show();
                                }

                            }',
                            'fileuploadfail' => 'function(e, data) {
                                console.log(e);
                                console.log(data);
                            }',
                        ],
                    ]);?>
                </div>

                <?php if (is_dir(Yii::getAlias('@webroot') . '/upload/company/'.$company_gallery_id.'/gallery/66')): ?>
                    <?php $images = scandir(Yii::getAlias('@webroot') . '/upload/company/'.$company_gallery_id.'/gallery/66'); ?>
                    <?php natsort($images); ?>
                    <div class="content-right-block-company-gallery__item-load"><img src='/images/loading.gif' /></div>
                    <div class="content-right-block-company-gallery__item-previews" data-images-count="<?=count($images) - 2?>">
                        <?php if (is_dir(Yii::getAlias('@webroot').'/upload/company/'.$model->id.'/gallery/66')): ?>
                        <?php foreach ($images as $image_key => $image): ?>
                            <?php if ($image_key > 1): ?>
                            <article data-company-id="<?=$model->id?>">
                                <span></span>
                                <img id="<?=explode('.', $image)[0]?>" src="/upload/company/<?=$model->id?>/gallery/66/<?=$image?>" alt=""/>
                            </article>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                <?php else: ?>
                    <div class="content-right-block-company-gallery__item-load"><img src='/images/loading.gif' /></div>
                    <div class="content-right-block-company-gallery__item-previews" data-images-count="0"></div>
                <?php endif; ?>
            </div>
        </div>

        <div class="content-right-block-company-address">
            <div class="content-right-block-company-address__item">
                <div class="content-right-block-company-label">Город <span class="star">*</span></div>
                <?= $form->field($model, 'city_id', ['template' => '<div class="select medium">{input}</div>'])->dropDownList(
                    ArrayHelper::map(City::find()->all(), 'id', 'name')
                ) ?>
            </div>
            <div class="content-right-block-company-address__item">
                <div class="content-right-block-company-label">Улица <span class="star">*</span></div>
                <?= $form->field($model, 'street')->textInput(['maxlength' => 255, 'placeholder' => 'Московское шоссе']) ?>
            </div>
            <div class="content-right-block-company-address__item">
                <div class="content-right-block-company-label">Дом <span class="star">*</span></div>
                <?= $form->field($model, 'house')->textInput(['maxlength' => 255, 'placeholder' => '42']) ?>
            </div>
            <div class="content-right-block-company-address__item">
                <div class="content-right-block-company-label">Офис</div>
                <?= $form->field($model, 'office')->textInput(['maxlength' => 255, 'placeholder' => '604']) ?>
            </div>
        </div>

        <div class="content-right-block-company-label">Основной телефон <span class="star">*</span></div>
        <?= $form->field($model, 'main_number')->textInput(['maxlength' => 255, 'class' => 'phone-mask']) ?>

        <div class="content-right-block-company-label">Дополнительный телефон</div>
        <?= $form->field($model, 'extra_number')->textInput(['maxlength' => 255, 'class' => 'phone-mask']) ?>

        <div class="content-right-block-company-label">Адрес сайта</div>
        <?= $form->field($model, 'site_url')->textInput(['maxlength' => 255, 'placeholder' => 'http://in-quality.ru',
            'class' => 'http-mask']) ?>

        <div class="content-right-block-company-label">Почта</div>
        <?= $form->field($model, 'email')->textInput(['maxlength' => 255, 'placeholder' => 'mail@in-quality.ru',
            'class' => 'email-mask']) ?>

        <div class="content-right-block-company-social">
            <div class="content-right-block-company-label">Страница в социальной сети</div>

            <?php if (!empty($social)): ?>
                <?php foreach ($social as $social_key => $social_value): ?>
                    <?php if (!empty($social_value)): ?>
                        <div class="content-right-block-company-social__item">
                            <div class="form-group">
                                <div class="select small-m">
                                    <select name="" id="" class="form-control">
                                        <option <?=($social_key=='vk')?'selected' : ''?> value="vk">Вконтакте</option>
                                        <option <?=($social_key=='fb')?'selected' : ''?> value="fb">Фейсбук</option>
                                        <option <?=($social_key=='tw')?'selected' : ''?> value="tw">Твиттер</option>
                                        <option <?=($social_key=='inst')?'selected' : ''?> value="inst">Инстаграм</option>
                                        <option <?=($social_key=='ok')?'selected' : ''?> value="ok">Одноклассники</option>
                                        <option <?=($social_key=='mail')?'selected' : ''?> value="mail">Mail.ru</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group"><?= Html::textInput('Company['.$social_key.']', $social_value) ?></div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
                <?php if (count($social) == 1): ?>
                    <div class="content-right-block-company-social__item">
                        <div class="form-group">
                            <div class="select small-m">
                                <select name="" id="" class="form-control">
                                    <option value="vk">Вконтакте</option>
                                    <option value="fb">Фейсбук</option>
                                    <option value="tw">Твиттер</option>
                                    <option value="inst">Инстаграм</option>
                                    <option value="ok">Одноклассники</option>
                                    <option value="mail">Mail.ru</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><?= Html::textInput('Company[vk]', '') ?></div>
                    </div>
                <?php endif; ?>
            <?php else: ?>
                <div class="content-right-block-company-social__item">
                    <div class="form-group">
                        <div class="select small-m">
                            <select name="" id="" class="form-control">
                                <option value="vk">Вконтакте</option>
                                <option value="fb">Фейсбук</option>
                                <option value="tw">Твиттер</option>
                                <option value="inst">Инстаграм</option>
                                <option value="ok">Одноклассники</option>
                                <option value="mail">Mail.ru</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group"><?= Html::textInput('Company[vk]', '') ?></div>
                </div>
                <div class="content-right-block-company-social__item">
                    <div class="form-group">
                        <div class="select small-m">
                            <select name="" id="" class="form-control">
                                <option value="vk">Вконтакте</option>
                                <option value="fb">Фейсбук</option>
                                <option value="tw">Твиттер</option>
                                <option value="inst">Инстаграм</option>
                                <option value="ok">Одноклассники</option>
                                <option value="mail">Mail.ru</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group"><?= Html::textInput('Company[vk]', '') ?></div>
                </div>
            <?php endif; ?>
        </div>
        <?php if (!empty($rubric)): ?>
            <div class="form-group field-companycompanycategory-0-company_id required">
                <?php foreach ($rubric as $rubric_key => $rubric_item): ?>
                    <?= Html::hiddenInput('CompanyCompanyCategory['.$rubric_key.'][company_id]', $rubric_item->company_id, [
                        'id' => 'companycompanycategory-'.$rubric_key.'-company_id'
                    ]) ?>
                <?php endforeach; ?>
            </div>
            <div class="form-group field-companycompanycategory-0-category_id required">
                <?php foreach ($rubric as $rubric_key => $rubric_item): ?>
                    <?= Html::hiddenInput('CompanyCompanyCategory['.$rubric_key.'][category_id]', $rubric_item->category_id, [
                        'id' => 'companycompanycategory-'.$rubric_key.'-category_id'
                    ]) ?>
                <?php endforeach; ?>
            </div>
            <div class="form-group field-companycompanycategory-0-subcategory_id required">
                <?php foreach ($rubric as $rubric_key => $rubric_item): ?>
                    <?= Html::hiddenInput('CompanyCompanyCategory['.$rubric_key.'][subcategory_id]', $rubric_item->subcategory_id, [
                        'id' => 'companycompanycategory-'.$rubric_key.'-subcategory_id'
                    ]) ?>
                <?php endforeach; ?>
            </div>
            <?php foreach ($rubric as $rubric_key => $rubric_item): ?>
                <?= Html::hiddenInput('CompanyCompanyCategory['.$rubric_key.'][id]', $rubric_item->id, [
                    'id' => 'companycompanycategory-'.$rubric_key.'-id'
                ]) ?>
            <?php endforeach; ?>
        <?php else: ?>
            <?= $form->field($company_company_category, '[0]company_id')->hiddenInput(['value' => $company_id]) ?>
            <?= $form->field($company_company_category, '[0]category_id')->hiddenInput() ?>
            <?= $form->field($company_company_category, '[0]subcategory_id')->hiddenInput() ?>
        <?php endif; ?>

        <?= $form->field($model, 'working_hours')->hiddenInput() ?>
        <?= $form->field($model, 'working_days')->hiddenInput() ?>
    </div>
</div>
<?= Html::submitButton('Сохранить', ['class' => 'button-save']) ?>

<?php ActiveForm::end(); ?>
