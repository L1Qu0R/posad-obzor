<?php

/* @var $this yii\web\View */
/* @var $model app\models\Company */

$this->title = 'Добавление компании / PosadObzor';
?>
<div class="content-right-title">Добавление компании</div>
<?= $this->render('_form', [
    'model' => $model,
]) ?>
<?= $this->render('/company/_modal') ?>
