<?php

use yii\widgets\ListView;
use app\helpers\PluralHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Поиск / PosadObzor';
$show_type = (isset($_COOKIE['show_type'])) ? $_COOKIE['show_type'] : '';
$small_active = ($show_type == 'small') ? 'active' : '';
$list_active = ($show_type == 'list' || $show_type == '') ? 'active' : '';

?>
<div class="modal" id="maps">
    <div class="modal-dialog">
        <div class="modal-close"></div>
        <div id="map" style="width: 600px; height: 400px;"></div>
    </div>
</div>
<div class="search-result">*Мы нашли для Вас <?=PluralHelper::pluralIt($dataProvider->getTotalCount(), [
        'компанию', 'компания', 'компании', 'компаний'
    ])?>.
</div>
<div class="show-types">
    <div class="view-type small <?=$small_active?>">
        <div class="view-type-icon">
            <div class="view-type-icon-item"></div>
            <div class="view-type-icon-item"></div>
            <div class="view-type-icon-item"></div>
            <div class="view-type-icon-item"></div>
        </div>
        <div class="view-type-text">Мелкие</div>
    </div>
    <div class="view-type list <?=$list_active?>">
        <div class="view-type-icon">
            <div class="view-type-icon-item"></div>
            <div class="view-type-icon-item"></div>
            <div class="view-type-icon-item"></div>
            <div class="view-type-icon-item"></div>
        </div>
        <div class="view-type-text">Список</div>
    </div>
</div>
<div class="companies-content search">
    <div class="companies-block <?=$show_type?>">
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_view',
            'layout' => "{items}\n{pager}",
            'pager' => [
                'prevPageLabel' => '',
                'nextPageLabel' => '',
            ],
            'emptyText' => 'Ничего не найдено',
        ]); ?>
    </div>
</div>
