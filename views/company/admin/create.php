<?php

/* @var $this yii\web\View */
/* @var $model app\models\Company */

?>
<div class="content-right-title">Добавление компании</div>
<?= $this->render('_form', [
    'model' => $model,
]) ?>
