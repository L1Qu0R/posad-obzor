<?php

use app\models\CompanyCategory;

$categories = CompanyCategory::find()->where(['parent_id' => 0])->all();
$subcategories = CompanyCategory::find()->where('parent_id != :parent_id', ['parent_id' => 0])->all();
$working_days = (isset($model)) ? json_decode($model->working_days) : array();

?>

<div class="modal" id="category">
    <div class="modal-dialog">
        <div class="modal-close"></div>
        <div class="categories active">
            <input type="text" placeholder="Начните вводить название категории" autofocus />
            <?php foreach ($categories as $category): ?>
                <div class="category" data-id="<?=$category->id?>"><?= $category->name ?></div>
            <?php endforeach; ?>
        </div>
        <div class="categories">
            <input data-parent-id="0" type="text" placeholder="Начните вводить название категории" autofocus />
            <?php foreach ($subcategories as $subcategory): ?>
                <div class="category" data-parent-id="<?=$subcategory->parent_id?>" data-id="<?=$subcategory->id?>">
                    <?= $subcategory->name ?>
                </div>
            <?php endforeach; ?>
            <div class="back">назад</div>
        </div>
    </div>
</div>

<div class="modal" id="working-days">
    <div class="modal-dialog">
        <div class="modal-close"></div>
        <div class="working-days-block">
            <div class="working-days-block-day">
                <input type="checkbox" name="checkbox_service"
                       class="checkbox <?=(in_array(1, $working_days)) ? 'checked' : ''?>"
                       id="1" <?=(in_array(1, $working_days)) ? 'checked' : ''?>/>
                <label for="1">Пн</label>
            </div>
            <div class="working-days-block-day">
                <input type="checkbox" name="checkbox_service"
                       class="checkbox <?=(in_array(2, $working_days)) ? 'checked' : ''?>"
                       id="2" <?=(in_array(2, $working_days)) ? 'checked' : ''?>/>
                <label for="2">Вт</label>
            </div>
            <div class="working-days-block-day">
                <input type="checkbox" name="checkbox_service"
                       class="checkbox <?=(in_array(3, $working_days)) ? 'checked' : ''?>"
                       id="3" <?=(in_array(3, $working_days)) ? 'checked' : ''?>/>
                <label for="3">Ср</label>
            </div>
            <div class="working-days-block-day">
                <input type="checkbox" name="checkbox_service"
                       class="checkbox <?=(in_array(4, $working_days)) ? 'checked' : ''?>"
                       id="4" <?=(in_array(4, $working_days)) ? 'checked' : ''?>/>
                <label for="4">Чт</label>
            </div>
            <div class="working-days-block-day">
                <input type="checkbox" name="checkbox_service"
                       class="checkbox <?=(in_array(5, $working_days)) ? 'checked' : ''?>"
                       id="5" <?=(in_array(5, $working_days)) ? 'checked' : ''?>/>
                <label for="5">Пт</label>
            </div>
            <div class="working-days-block-day">
                <input type="checkbox" name="checkbox_service"
                       class="checkbox <?=(in_array(6, $working_days)) ? 'checked' : ''?>"
                       id="6" <?=(in_array(6, $working_days)) ? 'checked' : ''?>/>
                <label for="6">Сб</label>
            </div>
            <div class="working-days-block-day">
                <input type="checkbox" name="checkbox_service"
                       class="checkbox <?=(in_array(7, $working_days)) ? 'checked' : ''?>"
                       id="7" <?=(in_array(7, $working_days)) ? 'checked' : ''?> />
                <label for="7">Вс</label>
            </div>
        </div>
    </div>
</div>