<?php

use \yii\helpers\Url;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мои компании / PosadObzor';
?>
<div class="content-right-title">Мои компании</div>
<?php if (isset($model)): ?>
<div class="content-right-block-company">
    <div class="content-right-block-company-hint">Теперь вы можете добавить свою компанию</div>
    <a href="<?=Url::toRoute([
        '/company/create', 'user_id' => Yii::$app->user->id, '#' => 'content'
    ])?>" class="button-ref button-right">Добавить</a>
</div>
<?php else: ?>
    <a href="<?=Url::toRoute([
        '/company/create', 'user_id' => Yii::$app->user->id, '#' => 'content'
    ])?>" class="button-ref button-right">Добавить</a>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => 'lk/_view',
        'layout' => "{items}\n{pager}",
    ]); ?>
<?php endif; ?>
