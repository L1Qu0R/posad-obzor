<?php

/* @var $this yii\web\View */
/* @var $model app\models\Company */

use app\models\City;
use app\models\CompanyCategory;
use app\helpers\DaysHelper;
use yii\helpers\Url;

$this->title = $model->name;
$working_hours = json_decode($model->working_hours);
$category = (isset(Yii::$app->request->get()['category'])) ?
    CompanyCategory::findOne(['alias' => Yii::$app->request->get()['category']]) : '';
$subcategory = (isset(Yii::$app->request->get()['subcategory']) && Yii::$app->request->get()['subcategory'] != '') ?
    CompanyCategory::findOne(['alias' => Yii::$app->request->get()['subcategory']]) : '';
$img_src = (file_exists(Yii::getAlias('@webroot') . '/upload/company/'.$model->id.'/logo.png')) ?
    '/upload/company/'.$model->id.'/logo.png' : '/images/company_blank.png';
$weekend = json_decode($model->working_days);
if (file_exists(Yii::getAlias('@webroot') . '/upload/company/'.$model->id.'/gallery')) {
    $gallery_images = scandir(Yii::getAlias('@webroot') . '/upload/company/'.$model->id.'/gallery');
    natsort($gallery_images);
}

?>
<div class="modal" id="maps">
    <div class="modal-dialog">
        <div class="modal-close"></div>
        <div id="map" style="width: 600px; height: 400px;"></div>
    </div>
</div>
<div class="modal" id="photo">
    <div class="modal-dialog">
        <div class="modal-close"></div>
        <div class="modal-arrow left"></div>
        <div class="modal-image"></div>
        <div class="modal-arrow right"></div>
    </div>
</div>
<div class="company-view">
    <div class="company-breadcrumbs">
        <div class="company-breadcrumbs-item"><a href="/">Главная</a></div>
        <div class="company-breadcrumbs-separator">/</div>
        <div class="company-breadcrumbs-item"><a href="<?=Url::toRoute(['/site/spravochnik'])?>">Справочник</a></div>
        <div class="company-breadcrumbs-separator">/</div>
        <?php if ($category != ''): ?>
            <div class="company-breadcrumbs-item"><a href="<?=
                Url::toRoute([
                    '/company/list',
                    'category' => $category->alias
                ])
                ?>"><?=$category->name?></a></div>
        <?php endif; ?>
        <?php if ($subcategory != ''): ?>
            <div class="company-breadcrumbs-separator">/</div>
            <div class="company-breadcrumbs-item"><a href="<?=
                Url::toRoute([
                    '/company/list',
                    'category' => $category->alias,
                    'subcategory' => $subcategory->alias,
                ])
                ?>"><?=$subcategory->name?></a></div>
        <?php endif; ?>
        <div class="company-breadcrumbs-separator">/</div>
        <div class="company-breadcrumbs-item"><?=$model->name?></div>
    </div>
    <div class="company-view-top">
        <div class="company-view-title">Информация о компании</div>
        <div class="company-view-edit_date">
            <div class="company-view-edit_date-text">Информация обновлена</div>
            <div class="company-view-edit_date-num"><?=date('d.m.Y', strtotime($model->e_date))?></div>
        </div>
    </div>
    <div class="company-view-left">
        <div class="company-view-photo_block">
            <div class="company-view-photo_block__logo">
                <span class="company-view-photo_block__logo-helper"></span>
                <img src="<?=$img_src?>" alt=""/>
            </div>
            <div class="company-view-photo_block__gallery">
                <div class="company-view-photo_block__gallery-wrapper">
                    <?php if (file_exists(Yii::getAlias('@webroot') . '/upload/company/'.$model->id.'/gallery')): ?>
                        <?php foreach($gallery_images as $image): ?>
                            <?php if (is_file(Yii::getAlias('@webroot') . '/upload/company/'.$model->id.'/gallery/' . $image) && ($image !== '.' && $image !== '..')): ?>
                            <div class="company-view-photo_block__gallery-item">
                                <img src="/upload/company/<?=$model->id?>/gallery/80/<?=$image?>" alt=""/>
                                <img src="/upload/company/<?=$model->id?>/gallery/<?=$image?>" alt=""/>
                            </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="company-view-photo_block__dots"></div>
        </div>
        <div class="company-view-info_block">
            <div class="company-view-info_block-name"><?= $model->name ?></div>
            <div class="company-view-info_block-description">
                <div class="company-view-info_block-description__short">
                    <?php if (mb_strlen($model->description, 'utf8') > 255): ?>
                        <?= $string = mb_substr($model->description, 0, 255, 'utf8') . '...' ?>
                    <?php else: ?>
                        <?= $model->description ?>
                    <?php endif; ?>
                </div>
                <div class="company-view-info_block-description__full">
                    <?= $model->description ?>
                </div>
            </div>
            <?php if (mb_strlen($model->description, 'utf8') > 255): ?>
                <div class="company-view-info_block-more">показать еще</div>
            <?php endif; ?>
            <div class="company-view-info_block-contacts">
                <div class="company-view-info_block-contacts-title">Контакты:</div>
                <div class="company-view-info_block-contacts-item phone">
                    <span>Телефон: </span>
                    <span><?= $model->main_number ?></span>
                </div>
                <div class="company-view-info_block-contacts-item address">
                    <span>Адрес: </span>
                <span>
                    г. <?= City::find($model->city_id)->one()->name ?>,
                    <?= $model->street ?>, д.<?= $model->house ?>
                </span>
                </div>
                <div class="company-view-info_block-contacts-item working-time">
                    <span>Время работы: </span>
                    <span>c <?= $working_hours->from ?> до <?= $working_hours->to ?></span>
                </div>
                <div class="company-view-info_block-contacts-item weekend">
                    <?php if (empty($weekend)): ?>
                        <span>Без выходных</span>
                    <?php else: ?>
                        <span>Вых: </span>
                        <span><?=join(', ', DaysHelper::replaceDays($weekend))?></span>
                    <?php endif; ?>
                </div>
                <?php if (!empty($model->site_url)): ?>
                    <div class="company-view-info_block-contacts-item site">
                        <span>Сайт: </span>
                        <span><a target="_blank" href="http://<?= $model->site_url ?>"><?= $model->site_url ?></a></span>
                    </div>
                <?php endif; ?>
                <div class="company-view-info_block-contacts-item social">
                    <?php if ($model->vk != ''): ?>
                        <a href="<?=$model->vk?>" target="_blank">
                            <div class="company-view-info_block-contacts-item__social vk"></div>
                        </a>
                    <?php endif; ?>
                    <?php if ($model->fb != ''): ?>
                        <a href="<?=$model->fb?>" target="_blank">
                            <div class="company-view-info_block-contacts-item__social fb"></div>
                        </a>
                    <?php endif; ?>
                    <?php if ($model->tw != ''): ?>
                        <a href="<?=$model->tw?>" target="_blank">
                            <div class="company-view-info_block-contacts-item__social tw"></div>
                        </a>
                    <?php endif; ?>
                    <?php if ($model->inst != ''): ?>
                        <a href="<?=$model->inst?>" target="_blank">
                            <div class="company-view-info_block-contacts-item__social inst"></div>
                        </a>
                    <?php endif; ?>
                    <?php if ($model->ok != ''): ?>
                        <a href="<?=$model->ok?>" target="_blank">
                            <div class="company-view-info_block-contacts-item__social ok"></div>
                        </a>
                    <?php endif; ?>
                    <?php if ($model->mail != ''): ?>
                        <a href="<?=$model->mail?>" target="_blank">
                            <div class="company-view-info_block-contacts-item__social mail"></div>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="company-view-right">
        <div class="company-view-right-map_block">
            <div id="static-map"></div>
            <div class="company-view-right-map_block-full_link show-map">увеличить карту</div>
        </div>
    </div>
</div>
