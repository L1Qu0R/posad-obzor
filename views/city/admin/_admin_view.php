<?php
use yii\helpers\Html;
?>
<tr>
    <td><?= $model->id ?></td>
    <td><?= $model->name ?></td>
    <td>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', array('/admin/default/update',
            'module' => 'city',
            'id' => $model->id)); ?>
    </td>
    <td>
        <?= Html::a('<span class="glyphicon glyphicon-remove"></span>', array('/admin/default/delete',
            'module' => 'city',
            'id' => $model->id),array(
            'class' => 'delete',
        )); ?>
    </td>
</tr>