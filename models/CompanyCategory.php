<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company_category".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property integer $parent_id
 *
 * @property Company[] $companies
 */
class CompanyCategory extends \yii\db\ActiveRecord
{
    const RUS_NAME = 'Категории';
    const MENU_SORT = 30;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id'], 'required'],
            [['parent_id'], 'integer'],
            [['name', 'alias'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Категория',
            'alias' => 'Алиас',
            'parent_id' => 'Родительская категория',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['subcategory_id' => 'id']);
    }
}
