<?php

namespace app\models;

use Yii;
use yii\base\Security;
use \yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $name
 * @property string $lastname
 * @property string $patronymic
 * @property string $email
 * @property string $salt
 * @property string $password
 * @property string $main_number
 * @property string $extra_number
 * @property string $vk
 * @property string $fb
 * @property string $tw
 * @property string $mail
 * @property string $c_date
 * @property string $auth_key
 * @property string $role
 */
class User extends ActiveRecord implements IdentityInterface
{
    public $repassword;
    public $avatar;

    const RUS_NAME = false;
    const MENU_SORT = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['c_date', 'password', 'repassword', 'auth_key'], 'safe'],
            [['name', 'email', 'password'], 'required', 'message' => 'Поле не может быть пустым!'],
            ['repassword', 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли должны совпадать!'],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Пользователь с таким email уже существует'],
            [['name', 'lastname', 'patronymic', 'email', 'salt', 'password', 'repassword', 'main_number', 'extra_number', 'vk', 'fb', 'tw', 'mail'], 'string', 'max' => 255],
            [['avatar'], 'file', 'extensions'=> ['png', 'jpg']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ВАШЕ ИМЯ:',
            'lastname' => 'ВАША ФАМИЛИЯ:',
            'patronymic' => 'ВАШЕ ОТЧЕСТВО:',
            'email' => 'ВАШ E-MAIL:',
            'salt' => 'Salt',
            'password' => 'ВАШ ПАРОЛЬ:',
            'repassword' => 'ПОВТОРИТЕ ПАРОЛЬ:',
            'main_number' => 'Main Number',
            'extra_number' => 'Extra Number',
            'vk' => 'Vk',
            'fb' => 'Fb',
            'tw' => 'Tw',
            'mail' => 'Mail',
            'c_date' => 'C Date',
            'auth_key' => 'C Date',
            'role' => 'Role',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // установка роли пользователя
        $auth = Yii::$app->authManager;
        $name = $this->role ? $this->role : 'user';
        $role = $auth->getRole($name);
        if (!$insert) {
            $auth->revokeAll($this->id);
        }
        $auth->assign($role, $this->id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by email
     *
     * @param  string      $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Finds user by password reset token
     *
     * @param  string      $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        $expire = \Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        if ($timestamp + $expire < time()) {
            // token expired
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @param  string  $db_password db password to validate
     * @param  string  $salt to password
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password, $db_password, $salt)
    {
        if (crypt($password, $salt) == $db_password) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Security::generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Security::generateRandomKey();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Security::generateRandomKey() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}
