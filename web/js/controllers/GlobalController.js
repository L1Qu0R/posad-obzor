var GlobalController = {

    init: function() {
        Global.init();
        GlobalController.controlModal();
        Global.startModal();

        $('.signup-block-tabs-tab').click(function() {
           Global.toggleTabs($('.signup-block-tabs-tab'), $('.signup-block-content-item'), $(this).index());
        });

        $(window).on('hashchange', function() {
            Global.startTabs($('.signup-block-tabs-tab'), $('.signup-block-content-item'),
                $('.signup-block-tabs-tab[href="'+window.location.hash+'"]').index());
        });

        $('.button-edit.edit_user_info').click(function() {
           Global.toggleInputs();
        });

        $('body').click(function() {
            if (!$('.content-right:hover').length > 0) {
                $('.content-right-block-user-right').removeClass('active');
            }
        });

        $('.checkbox').next('label').click(function() {
           $(this).prev('.checkbox').toggleClass('checked');
        });

        $('.rubric-remove').click(function() {
            Global.removeRubric($(this));
        });

        $('.image-upload').change(function() {
            var input = event.target;

            var reader = new FileReader();
            reader.onload = function(){
                var dataURL = reader.result;
                var output = document.getElementById('logo-img');
                output.src = dataURL;
            };
            reader.readAsDataURL(input.files[0]);
        });

        $('.header-content-toggles-toggle.menu, .header-under_menu').hover(
            function(){$('.header-under_menu').addClass('active')},
            function(){$('.header-under_menu').removeClass('active')}
        );

        $('.header-under_menu-toggle').click(function() {
            $('.header-under_menu').toggleClass('slide');
        });
    },

    controlModal: function() {
        $('.modal-dialog').hover(
            function() {$(this).addClass('hover');},
            function() {$(this).removeClass('hover');}
        );

        $('.modal').click(function() {
            if(!$('.modal-dialog').hasClass('hover')) {
                $('.modal').removeClass('active');
                $('body').removeClass('modaled');
                history.pushState("", document.title, window.location.pathname);
            }
        });

        $('.modal-close').click(function() {
            $('.modal').removeClass('active');
            $('body').removeClass('modaled');
            history.pushState("", document.title, window.location.pathname);
        });

        $('.open-modal').click(function() {
            $('.modal#' + $(this).data('modalId')).addClass('active');
            $('body').addClass('modaled');
        });
    }

};