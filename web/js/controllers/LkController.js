var LkController = {

    init: function() {
        Lk.init();

        $('.modal#category .categories').eq(0).find('.category').click(function() {
           Lk.insertCategory($(this));
        });

        $('.modal#category .categories').eq(1).find('.category').click(function() {
           Lk.insertSubCategory($(this));
        });

        $('.modal#category .categories .back').click(function() {
            var categories = $('.modal#category .categories');
            categories.eq(1).removeClass('active');
            categories.eq(0).addClass('active');
            categories.find('.category').show();
        });

        $('.modal#category input').on('input', function() {
           Lk.searchCategory($(this));
        });

        $('.content-right-block-company-mode__item .button-ref').click(function() {
            Lk.insertWorkingHours();
        });

        $('body').on('click', '.open-lk-modal', function() {
            Lk.toggleModal($(this).data('modalId'));
            if ($(this).data('modalId') == 'category') {
                $('#category').attr('data-rubric-id', $(this).parents('.rubric').attr('id'));
            }
        });

        $('.content-right-block-company-category__append').on('click', function() {
            Lk.appendRubric($(this));
        });

        $('.working-days-block-day').click(function() {
           Lk.insertWorkingDays();
        });

        $('.content-right-block-company-social__item select').change(function() {
            Lk.insertSocial($(this));
        });

        $('.content-right-block-user-left-photo input').change(function() {
            $(this).parents('form').submit();
        });

        $('#company-gallery_image').change(function() {
            Lk.startLoadAnimation();
        });

        $('body').on('click', '.content-right-block-company-gallery__item-previews article span', function() {
            Lk.removeGalleryImage($(this));
        });
    }

};