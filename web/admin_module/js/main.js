var head = $('head');

//main scripts
head.append('<script src="/admin_module/js/models/Admin.js"></script>');
head.append('<script src="/admin_module/js/controllers/AdminController.js"></script>');

$(document).ready(function() {
    AdminController.init();
    GlobalController.init();
    LCalendar.init('ru');

    $('#example').dataTable(
        {
            "order": [[0, "desc"]],
            "language": {
                "lengthMenu": "Отображать _MENU_ записей на странице",
                "zeroRecords": "Ничего не найдено - извините",
                "info": "Показана страница _PAGE_ из _PAGES_",
                "infoEmpty": "Записей не найдено",
                "infoFiltered": "(Отфильтровано из _MAX_ записей)",
                "search":         "Поиск: ",
                "paginate": {
                    "first":      "Первая",
                    "last":       "Последняя",
                    "next":       "Далее",
                    "previous":   "Назад"
                }
            },
            "iDisplayLength": 100
        }
    );

    CKEDITOR.replace('ckeditor');
});